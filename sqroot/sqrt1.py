import sys
import itertools

__author__ = 'rzurga'

EPS = 0.000000000000001

def sqrt1(n):
    x = n
    y = n + 2.0 * EPS
    i = 0
    while abs(x - y) > EPS:
        y = x
        x = (x + n / x) / 2.0
        i += 1
    return (x, i)

def main(argv):
    print "main: {}".format(argv)
    for arg in argv:
        n = float(arg)
        sq, k = sqrt1(n)
        print "sqrt :=> {} ({})".format(sq, k)
        sqf = reduce(lambda x,y: (x + n / (x if x != 0.0 else 1.0)) / 2.0, itertools.takewhile(lambda m: m < 41, xrange(1000)))
        print "sqrtfunc :=> {}".format(sqf)

if __name__ == "__main__":
    main(sys.argv[1:])
